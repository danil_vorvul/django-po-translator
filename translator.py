import googletrans
import os
from googletrans import Translator

### SETUP ###
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
src_language = "ru"
dest_language = "uk"
src_file_path = f"{BASE_DIR}/backend/locale/{src_language}/LC_MESSAGES/django.po"
res_file_path = f"{BASE_DIR}/backend/locale/{dest_language}/LC_MESSAGES/django.po"
translator = Translator()

### MAIN PART ###
with open(res_file_path, "w") as res:
    with open(f"{src_file_path}", "r") as src:
        for line in src:
            if line.find("msgid") != -1:
                res.write(line)
            elif line.find("msgstr") != -1:
                splitted_str = line.split('"')
                translated = translator.translate(
                    splitted_str[1], src=src_language, dest=dest_language
                )
                res.write(f'msgstr "{translated.text.capitalize()}"')
                res.write("\n\n")
