# django-po-translator

This script help you to automate translations django .po files with google API. 


## How to use ?

You need to specify variables in setup blog manually (I want to make it pip package later, it`s just 0.0.0.0.1 version :>):
```
src_language = "ru"
dest_language = "uk"
src_file_path = f"{BASE_DIR}/backend/locale/{src_language}/LC_MESSAGES/django.po"
res_file_path = f"{BASE_DIR}/backend/locale/{dest_language}/LC_MESSAGES/django.po"
```

Than just run script from terminal:
```
python translator.py
```

Enjoy! :D